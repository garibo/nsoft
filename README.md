# NSoft
 This project is ....

## I. Backend - microservices - Docker/ks8
- [ ] **CQRS** - Command Query Responsibility Segregation command patern 
- [ ] **WebApi** - Service in .Net Core Framework
- [ ] **NUnit Test** - integration,e2 and unit tests  
- [x] **Docker** - container services builder/publisher
- [ ] **CI/D scripts** 
- [ ] **Swagger** - UI provides a display framework that reads an OpenAPI specification document and generates an interactive documentation website 

## Database
- [ ] Redis ?
- [ ] DacPac ?


## Backend stack
**Services:**

- _NSoft.Configuration.Api_

## Setup
**Docker Compose**

Run powershell script

` ./app-develop-stack.ps1 `

   or 
   
` docker-compose -f apps-develope-compose.yml up --build -d `

