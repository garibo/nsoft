﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using SharedFunctionality.Swagger.Options;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace SharedFunctionality.Swagger.Extensions
{
    public static class ServiceCollectionExtensions
    {
        private const string HEADER_AUTH_NAME = "Authorization";
        private const string BEARER_SCHEME_NAME = "Bearer";
        
        private static string _assemblyName = Assembly.GetEntryAssembly()?.GetName().Name;

        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, Action<SwaggerOptions> configurationOptions)
        {
            var configuration = new SwaggerOptions();
            configurationOptions?.Invoke(configuration);

            AddApiVersion(services);

            services.AddSwaggerGen(swagger =>
                {
                    if (configuration.ApiDocuments?.Any() ?? false)
                    {
                        foreach (var security in configuration.SecurityDefinition)
                            swagger.AddSecurityDefinition(security.Key, security.Value);

                        foreach (var requirement in configuration.SecurityRequirements)
                            swagger.AddSecurityRequirement(requirement);

                        var provider = services.BuildServiceProvider()
                            .GetRequiredService<IApiVersionDescriptionProvider>();

                        foreach (var description in provider.ApiVersionDescriptions)
                        {
                            configuration.ApiDocuments.TryGetValue(description.GroupName, out var apiDocument);
                            swagger.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description, apiDocument));
                        }

                        swagger.ResolveConflictingActions(p => p.First());
                        swagger.DescribeAllParametersInCamelCase();
                        swagger.OrderActionsBy(p => p.HttpMethod);

                        if (configuration.EnableBearerAuth)
                            AddJwtAuthorization(swagger);
                    }

                    var xmlPath = Path.Combine(AppContext.BaseDirectory, $"{_assemblyName}.xml");

                    if (File.Exists(xmlPath))
                        swagger.IncludeXmlComments(xmlPath);
                }
            );

            return services;
        }

        private static void AddApiVersion(IServiceCollection services)
        {
            services.AddApiVersioning(options =>
            {
                options.UseApiBehavior = true;
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
                options.AssumeDefaultVersionWhenUnspecified = true;
            });
            services.AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
        }

        private static void AddJwtAuthorization(SwaggerGenOptions swagger)
        {
            var openApiSecurity = new OpenApiSecurityScheme
            {
                Description =
                    "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                Name = HEADER_AUTH_NAME,
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
            };

            swagger.AddSecurityDefinition(BEARER_SCHEME_NAME, openApiSecurity);
            swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = BEARER_SCHEME_NAME
                        }
                    },
                    new string[] { }
                }
            });
        }

        static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description, OpenApiInfo apiDocument)
        {
            var info = new OpenApiInfo()
            {
                Title = $"{apiDocument.Title} {description.GroupName}",
                Version = description.ApiVersion.ToString(),
                Description = apiDocument.Description,
            };

            if (description.IsDeprecated)
                info.Description += " [This API version has been deprecated] .";

            return info;
        }
    }
}
