﻿using SharedFunctionality.Swagger.Options;
using SharedFunctionality.Swagger.Validators;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using ConfigObject = Swashbuckle.AspNetCore.ReDoc.ConfigObject;

namespace SharedFunctionality.Swagger.Extensions
{
    public static class ApplicationBuilderExtension
    {
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app, Action<SwaggerSetupConfiguration> swaggerConfiguration = default)
        {
            var configuration = new SwaggerSetupConfiguration();
            swaggerConfiguration?.Invoke(configuration);

            var apiVersionDescriptionProvider = app.ApplicationServices.GetService<IApiVersionDescriptionProvider>();
            var swaggerDoc = app.ApplicationServices.GetService<SwaggerOptions>();

            app.UseSwagger(options => options.RouteTemplate = configuration.RouteTemplate);

            if (configuration.EnableUI)
                app.UseSwaggerUI(options =>
                {
                    options.EnableDeepLinking();
                    options.DefaultModelExpandDepth(-1);
                    options.RoutePrefix = configuration.RoutePrefix;

                    if (IsValid(configuration.OAuthOptions))
                    {
                        options.OAuthClientId(configuration.OAuthOptions.ClientId);
                        options.OAuthAppName(configuration.OAuthOptions.AppName);
                        options.OAuthAdditionalQueryStringParams(configuration.OAuthOptions.AdditionalQueryParams);
                    }

                    if (apiVersionDescriptionProvider?.ApiVersionDescriptions == null) return;

                    foreach (var api in apiVersionDescriptionProvider?.ApiVersionDescriptions)
                        options.SwaggerEndpoint(string.IsNullOrEmpty(configuration.RouteTemplatePrefix) ? $"/{api.GroupName}/swagger.json" : $"/{configuration.RouteTemplatePrefix}/{api.GroupName}/swagger.json", api.GroupName);
                });

            if (!configuration.UseReDocUI) return app;

            if (configuration.UseReDocUI && !swaggerDoc.UseRedoc)
                throw new InvalidOperationException("To enable use ReDocUI is required set 'UseReDoc = true' in Swagger service collection setup.");

            app.UseReDoc(c =>
                {
                    c.RoutePrefix = "api-docs";
                    c.SpecUrl = $"/{configuration.RouteTemplatePrefix}/{apiVersionDescriptionProvider.ApiVersionDescriptions.FirstOrDefault()?.GroupName}/swagger.json";
                    c.ConfigObject = new ConfigObject
                    {
                        HideDownloadButton = true,
                        HideLoading = true
                    };
                });

            return app;
        }

        private static bool IsValid(OAuthOptions options)
        {
            if (options == null)
                return false;
            var validator = new OAuthOptionsValidator();
            var result = validator.Validate(options);

            return result.IsValid;
        }
    }
}