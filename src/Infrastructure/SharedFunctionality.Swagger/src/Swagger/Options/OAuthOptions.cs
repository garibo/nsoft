﻿using System.Collections.Generic;

namespace SharedFunctionality.Swagger.Options
{
    public class OAuthOptions
    {
        public string ClientId { get; set; }
        public string AppName { get; set; }
        public Dictionary<string, string> AdditionalQueryParams { get; set; } = new Dictionary<string, string>();
        public string AuthorizationUrl { get; set; }
        public string TokenUrl { get; set; }
    }
}
