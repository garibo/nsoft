﻿namespace SharedFunctionality.Swagger.Options
{
    public class SwaggerSetupConfiguration
    {
        public bool EnableUI { get; set; }
        public bool UseReDocUI { get; set; }
        /// <summary>
        /// RouteTemplater set up the path where swagger.json is available. Changing RouteTemplatePrefix will aslo effect on RouteTemplate by adding each request e.g. /{RouteTemplatePrefix}/v1/swagger.json
        /// <para>Default value is '/{RouteTemplatePrefix}/{documentName}/swagger.json'</para>
        /// </summary>
        public string RouteTemplate { get; set; } = "{documentName}/swagger.json";

        /// <summary>
        /// RouteTemplatePrefix is set up WebApplicationName per request for swagger document e.g. /#{WebApplicationName}/v1/swagger.json
        /// </summary>
        public string RouteTemplatePrefix { get; set; } = "";

        /// <summary>
        /// RoutePrefix append before each request e.g. /'swagger'/index.html
        /// </summary>
        public string RoutePrefix { get; set; } = "swagger";

        public OAuthOptions OAuthOptions { get; set; }
    }
}
