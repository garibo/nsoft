﻿using System.Collections.Generic;
using Microsoft.OpenApi.Models;

namespace SharedFunctionality.Swagger.Options
{
    public class SwaggerOptions
    {
        public bool UseRedoc { get; set; }
        public bool EnableBearerAuth { get; set; }
        public IDictionary<string,OpenApiInfo> ApiDocuments { get; set; } = new SortedDictionary<string, OpenApiInfo>();
        public IDictionary<string, OpenApiSecurityScheme> SecurityDefinition { get; set; } = new SortedDictionary<string, OpenApiSecurityScheme>();
        public List<OpenApiSecurityRequirement> SecurityRequirements { get; set; } = new List<OpenApiSecurityRequirement>();
    }
}
