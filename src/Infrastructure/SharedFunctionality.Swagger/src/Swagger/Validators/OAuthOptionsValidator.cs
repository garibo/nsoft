﻿using FluentValidation;
using SharedFunctionality.Swagger.Options;

namespace SharedFunctionality.Swagger.Validators
{
    internal class OAuthOptionsValidator : AbstractValidator<OAuthOptions>
    {
        public OAuthOptionsValidator()
        {
            RuleFor(p => p.AppName).NotEmpty();
            RuleFor(p => p.ClientId).NotEmpty();
            RuleFor(p => p.AuthorizationUrl).NotEmpty();
            RuleFor(p => p.TokenUrl).NotEmpty();
        }
    }
}
