﻿namespace NSoft.Configuration.Interfaces.Commands.v1
{
    public class CreateGroupCommand
    {
        public CreateGroupCommand(string tabName, int order, int langId, string description)
        {
            TabName = tabName;
            Order = order;
            LangId = langId;
            Description = description;
        }

        public string TabName { get; set; }
        public int Order { get; set; }
        public int LangId { get; set; }
        public string Description { get; set; }
    }
}
