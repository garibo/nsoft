﻿using System;

namespace NSoft.Configuration.Interfaces.Response
{
    public class GroupsQueryResponse
    {
        public Guid Id { get; set; }
        public string TabName { get; set; }
        public int Order { get; set; }
        public int LangId { get; set; }
        public string Description { get; set; }
    }
}
