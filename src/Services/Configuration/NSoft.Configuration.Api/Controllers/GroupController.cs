﻿using Microsoft.AspNetCore.Mvc;
using NSoft.Configuration.Interfaces.Commands.v1;
using SharedFunctionality.Cqrs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NSoft.Configuration.Api.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{v:apiVersion}/[controller]/[action]")]
    public class GroupController : Controller
    {
        private readonly IGate _gate;

        public GroupController(IGate gate)
        {
            _gate = gate;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateGroupCommand command, CancellationToken token)
        {
            var result = await _gate.ExecuteCommandAsync<CreateGroupCommand, int>(command, token);

            return Ok(result);
        }
    }
}
