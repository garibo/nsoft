﻿using AutoMapper;
using NSoft.Configuration.Api.Domain;
using NSoft.Configuration.Interfaces.Commands.v1;
using NSoft.Configuration.Interfaces.Response;
using System;

namespace NSoft.Configuration.Api.Application.Mapper
{
    public class DomainMapper : Profile
    {
        public DomainMapper()
        {
            CreateMap<Group, GroupsQueryResponse>().ReverseMap();
        }
    }
}
