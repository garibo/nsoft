﻿using AutoMapper;
using NSoft.Configuration.Api.Infrastructure.Repositories;
using NSoft.Configuration.Interfaces.Queries.v1;
using NSoft.Configuration.Interfaces.Response;
using SharedFunctionality.Cqrs.Handler.Queries;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NSoft.Configuration.Api.Application.Queries
{
    internal class GetGroupsQueryHandler : IAsyncQueryHandler<GetGroupsQuery, List<GroupsQueryResponse>>
    {
        private readonly IGroupRepository _repository;
        private readonly IMapper _mapper;

        public GetGroupsQueryHandler(IGroupRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<List<GroupsQueryResponse>> Handle(GetGroupsQuery request, CancellationToken token = default)
        {
            var groupsList = await _repository.GetAll();

            if (groupsList == default) return null;

            var result = _mapper.Map<List<GroupsQueryResponse>>(groupsList);

            return result;
        }
    }
}
