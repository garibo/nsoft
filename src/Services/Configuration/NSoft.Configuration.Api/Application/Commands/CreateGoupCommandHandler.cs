﻿using NSoft.Configuration.Api.Domain;
using NSoft.Configuration.Api.Infrastructure.Repositories;
using NSoft.Configuration.Interfaces.Commands.v1;
using SharedFunctionality.Cqrs.Handler.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace NSoft.Configuration.Api.Application.Commands
{
    internal class CreateGoupCommandHandler : IAsyncCommandHandler<CreateGroupCommand, int>
    {
        private readonly IGroupRepository _repository;

        public CreateGoupCommandHandler(IGroupRepository  repository)
        {
            _repository = repository;
        }

        public async Task<int> Handle(CreateGroupCommand command, CancellationToken token = default)
        {
            var group = new Group(default, command.TabName, command.Order, command.LangId, command.Description);

            var result = await _repository.Create(group);

            return result;
        }
    }
}
