﻿using FluentValidation;
using NSoft.Configuration.Interfaces.Commands.v1;

namespace NSoft.Configuration.Api.Application.Commands.Validators
{
    public class CreateGroupCommandValidator : AbstractValidator<CreateGroupCommand>
    {
        public CreateGroupCommandValidator()
        {
            RuleFor(p => p.TabName).NotEmpty();
            RuleFor(p => p.Description).NotEmpty();
            RuleFor(p => p.Order).GreaterThan(0);
        }
    }
}
