using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using NSoft.Configuration.Api.Infrastructure.Extensions;
using NSoft.Configuration.Api.Infrastructure.Filters;
using SharedFunctionality.ExceptionHandling.Exceptions;
using SharedFunctionality.Swagger.Extensions;
using System.Collections.Generic;
using System.Reflection;

namespace NSoft.Configuration.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddConfigurationServices(_configuration, Assembly.GetExecutingAssembly());

            services.AddSwaggerDocumentation(options =>
            {
                options.ApiDocuments.TryAdd("v1", new OpenApiInfo { Title = "NSoft Configuration WebApi Document ", Version = "V1" });
            });

            services.AddMvc(optons => optons.Filters.Add<ModelStateValidationFilter>())
                    .AddFluentValidation(options =>
                    {
                        options.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                        options.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    })
                    .AddNewtonsoftJson();

            services.Configure<ApiBehaviorOptions>(behaviors => behaviors.SuppressModelStateInvalidFilter = true);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseSwaggerDocumentation(options => options.EnableUI = true);

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
