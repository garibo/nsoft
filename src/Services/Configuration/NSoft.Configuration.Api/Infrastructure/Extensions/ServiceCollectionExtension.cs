﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSoft.Configuration.Api.Infrastructure.Atributes;
using System;
using System.Data;
using System.Reflection;
using SharedFunctionality.Cqrs.DI;

namespace NSoft.Configuration.Api.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        private static Type[] attributes =
        {
            typeof(ProviderAttribute),
            typeof(SerializableAttribute),
            typeof(RepositoryAttribute)
        };

        public static void AddConfigurationServices(this IServiceCollection services, IConfiguration configuration, params Assembly[] assemblies)
        {
            services.AddScoped<IDbConnection>(options =>
            {
                var connectionsString = configuration.GetConnectionString("Test");
                return new SqlConnection(connectionsString);
            });

            services.AddCqrs(options=> options.UseRequestLogger = true);

            AddImplementedInterfaces(services, assemblies);

            services.AddAutoMapper(assemblies);
        }

        private static void AddImplementedInterfaces(IServiceCollection services, params Assembly[] assemblies)
        {
            Array.ForEach(attributes, attribute =>
            {
                services.Scan(scan =>
                {
                    scan.FromAssemblies(assemblies).AddClasses(classes => classes.WithAttribute(attribute))
                    .AsImplementedInterfaces().WithScopedLifetime();
                });
            });
        }
    }
}
