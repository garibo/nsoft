﻿using NSoft.Configuration.Api.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NSoft.Configuration.Api.Infrastructure.Repositories
{
    public interface IGroupRepository
    {
        Task<IList<Group>> GetAll();
        Task<Group> GetBy(Guid groupId);
        Task<int> Create(Group group);
    }
}