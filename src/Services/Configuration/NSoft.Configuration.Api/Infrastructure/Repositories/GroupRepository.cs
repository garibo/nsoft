﻿using Dapper;
using NSoft.Configuration.Api.Domain;
using NSoft.Configuration.Api.Infrastructure.Atributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace NSoft.Configuration.Api.Infrastructure.Repositories
{
    [Repository]
    public class GroupRepository : IGroupRepository
    {
        private readonly IDbConnection _connection;

        public GroupRepository(IDbConnection dbConnection)
        {
            _connection = dbConnection;
        }

        public async Task<IList<Group>> GetAll()
        {
            var queryData = await _connection.QueryAsync<Group>("dbo.spc_Group_GetAll", commandType: CommandType.StoredProcedure);

            var result = queryData.AsList();

            return result ?? null;
        }

        public async Task<Group> GetBy(Guid groupId)
        {
            var result = await _connection.QueryFirstAsync<Group>("dbo.spc_Group_GetByGroupId", new { groupId }, commandType: CommandType.StoredProcedure);

            return result;
        }

        public async Task<int> Create(Group group)
        {
         
        }
    }
}
