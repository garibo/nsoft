﻿using Microsoft.AspNetCore.Mvc.Filters;
using SharedFunctionality.ExceptionHandling.Exceptions;
using System.Linq;
using System.Threading.Tasks;

namespace NSoft.Configuration.Api.Infrastructure.Filters
{
    public class ModelStateValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
           if(!context.ModelState.IsValid)
            {
                var errors = string.Join("; ", context.ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage));
                throw new ValidationException(errors);
            }

            await next();
        }
    }
}
