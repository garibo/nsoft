﻿using System;

namespace NSoft.Configuration.Api.Domain
{
    public class Section
    {
        private Section() { }

        public Section(int id, string tabName, int order, int langId, string description)
        {
            Id = id;
            TabName = tabName;
            Order = order;
            LangId = langId;
            Description = description;
        }

        public int Id { get; }
        public string TabName { get; }
        public int Order { get; }
        public int LangId { get; }
        public string Description { get; }
    }
}
