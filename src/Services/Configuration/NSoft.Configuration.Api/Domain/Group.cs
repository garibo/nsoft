﻿using System;

namespace NSoft.Configuration.Api.Domain
{
    public class Group
    {
        private Group() { }

        public Group(int id, string tabName, int order, string langId, string description)
        {
            Id = id;
            TabName = tabName;
            Order = order;
            LangId = langId;
            Description = description;
        }

        public int Id { get; }
        public string TabName { get; }
        public int Order { get; }
        public string LangId { get; }
        public string Description { get; }
    }
}
