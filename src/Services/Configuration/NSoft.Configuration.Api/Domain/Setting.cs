﻿using NSoft.Configuration.Api.Domain.Shared;
using System;

namespace NSoft.Configuration.Api.Domain
{
    public class Setting
    {
        private Setting() { }

        public Setting(int id, string name, string dataValue,
            int dictId, int langId, FlagTypesEnum flagType, bool flag, string[] additionalItems, Version version)
        {
            Id = id;
            Name = name;
            DataValue = dataValue;
            DictId = dictId;
            LangId = langId;
            FlagType = flagType;
            Flag = flag;
            AdditionalItems = additionalItems;
            Version = version;
        }

        public int Id { get; }
        public string Name { get; }
        public string DataValue { get; set; }
        public int DictId { get; }
        public int LangId { get; }
        public FlagTypesEnum FlagType { get; }
        public bool Flag { get; }
        public string[] AdditionalItems { get; }
        public Version Version { get; }
    }
}
