﻿using System;

namespace NSoft.Configuration.Api.Domain.Shared
{
    [Flags]
    public enum FlagTypesEnum
    {
        None,
        Int,
        Bool,
        Date,
        List,
        Dict
    }
}
