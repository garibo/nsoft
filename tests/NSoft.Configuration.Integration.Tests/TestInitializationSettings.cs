using NSoft.Configuration.Integration.Tests.Infrastructure;
using NUnit.Framework;
using System;
using System.Net.Http;

namespace NSoft.Configuration.Integration.Tests
{
    [SetUpFixture]
    public class TestInitializationSettings
    {
        public static IServiceProvider Services;
        public static HttpClient HttpClient;

        [OneTimeSetUp]
        public void Setup()
        {
            var dacFile = "../../../../../src/Infrastructure/NSoft.Configuration.Database/bin/NSoft.Configuration.dacpac";

            DbServiceDeploy.Deploy(dacFile);

            Services = NSoftConfigurationTestServer.Services;
            HttpClient = NSoftConfigurationTestServer.Create("http://local.configuration");
        }

        [OneTimeTearDown]
        public void Down()
        {
            HttpClient?.Dispose();
        }
    }
}
