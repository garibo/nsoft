﻿using NSoft.Configuration.Integration.Tests.Infrastructure;
using NSoft.Configuration.Interfaces.Commands.v1;
using NUnit.Framework;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NSoft.Configuration.Integration.Tests.Presentation
{
    [TestFixture(Category = "Intergration")]
    public class GroupControllerTest
    {
        private readonly HttpClient _httpClient;
        private const string BASE_PATH = "/api/v1/Group";
        public GroupControllerTest()
        {
            _httpClient = TestInitializationSettings.HttpClient;
        }


        [Test]
        public async Task Test()
        {
            var command = new CreateGroupCommand ("Test", 1, 1, "Test Group");

           var response = await _httpClient.PostAsJsonAsync($"{BASE_PATH}/Create", command);

            response.EnsureSuccessStatusCode();

            var groupId = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(int.Parse(groupId) > 0);
        }
    }
}
