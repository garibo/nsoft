﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NSoft.Configuration.Api;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Server = Microsoft.AspNetCore.TestHost.TestServer;


namespace NSoft.Configuration.Integration.Tests.Infrastructure
{
    public class NSoftConfigurationTestServer
    {
        public static IServiceProvider Services;
        private static readonly HttpClient HttpClient;

        static NSoftConfigurationTestServer()
        {
            var server = new Server(WebHost.CreateDefaultBuilder()
                .UseEnvironment("Test")
                .UseStartup<Startup>());

            Services = server.Services;
            HttpClient = server.CreateClient();
        }

        public static HttpClient Create(string baseAddress, string authotizationToken = null)
        {
            HttpClient.BaseAddress = new Uri(baseAddress);

            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrEmpty(authotizationToken))
                HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authotizationToken);

            return HttpClient;
        }
    }
}
