﻿using Microsoft.SqlServer.Dac;
using System.IO;

namespace NSoft.Configuration.Integration.Tests.Infrastructure
{
    public class DbServiceDeploy
    {
        public static string ConnectionString =
                "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=NSoft.Configuration;Integrated Security=True;Connect Timeout=0;Encrypt=False;TrustServerCertificate=True; MultipleActiveResultSets=True";

        public static void Deploy(string dacpacFilePath)
        {
            var dacServices = new DacServices(ConnectionString);
            var options = new DacDeployOptions
            {
                CreateNewDatabase = true,
                BlockOnPossibleDataLoss = false,
                GenerateSmartDefaults = true,
                VerifyDeployment = true,
                RunDeploymentPlanExecutors = true
            };

            if (!File.Exists(dacpacFilePath)) throw new FileNotFoundException("File not exists", dacpacFilePath);

            var dacpackPackage = DacPackage.Load(dacpacFilePath);
            dacServices.Deploy(dacpackPackage, "NSoft.Configuration", true, options);
        }
    }
}
